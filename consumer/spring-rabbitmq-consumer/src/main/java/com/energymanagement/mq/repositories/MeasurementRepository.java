package com.energymanagement.mq.repositories;


import com.energymanagement.mq.entities.Measurement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface MeasurementRepository extends JpaRepository<Measurement, Long> {

}
