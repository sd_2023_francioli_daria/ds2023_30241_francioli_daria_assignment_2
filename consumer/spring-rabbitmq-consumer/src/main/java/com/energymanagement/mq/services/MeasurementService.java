package com.energymanagement.mq.services;


import com.energymanagement.mq.entities.Device;
import com.energymanagement.mq.entities.Measurement;
import com.energymanagement.mq.repositories.MeasurementRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class MeasurementService {

    MeasurementRepository measurementRepository;

    @Autowired
    public MeasurementService(MeasurementRepository measurementRepository){
        this.measurementRepository = measurementRepository;
    }

    public List<Measurement> findAll() {
        List<Measurement> measurements = measurementRepository.findAll();
        return measurements;
    }



    public Measurement findById(Long id) {
        Optional<Measurement> measurementOptional = measurementRepository.findById(id);
        if (!measurementOptional.isPresent()) {
            throw new RuntimeException(Measurement.class.getSimpleName() + " with id: " + id);
        }
        return measurementOptional.get();
    }

    public Measurement insert(Measurement measurement  ) {
        Measurement newMeasurement = measurementRepository.save(measurement);
        if(measurement==null)
        {
            System.out.println("error");
        }
        return newMeasurement;
    }

    //TODO: update, delete

    public Measurement update(Long id, Measurement measurement) {
        Optional<Measurement> measurementOptional = measurementRepository.findById(id);
        if(measurementOptional.isEmpty()){
            return null;
        }
        Measurement updateMeasurement = measurementOptional.get();
        updateMeasurement.setTimestamp(measurement.getTimestamp());
        updateMeasurement.setMeasurement_value(measurement.getMeasurement_value());
        updateMeasurement.setDevice_id(measurement.getDevice_id());
        updateMeasurement = measurementRepository.save(updateMeasurement);
        return updateMeasurement;
    }

    public Measurement delete(Long id) {
        Measurement deleteMeasurement = findById(id);
        if (deleteMeasurement!=null)
        {
            measurementRepository.delete(deleteMeasurement);
        }
        return deleteMeasurement;
    }
}
