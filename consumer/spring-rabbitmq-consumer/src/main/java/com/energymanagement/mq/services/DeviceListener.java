package com.energymanagement.mq.services;

import com.energymanagement.mq.config.MQConfig;
import com.energymanagement.mq.entities.CustomMessage;
import com.energymanagement.mq.entities.Device;
import com.energymanagement.mq.entities.Measurement;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class DeviceListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageListener.class);
    private final DeviceService deviceService;


    @Autowired
    public DeviceListener(DeviceService deviceService) {
        this.deviceService = deviceService;
    }


    @RabbitListener(queues = MQConfig.QUEUE_DEVICE)
    public void listener(String message)  {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Device device = objectMapper.readValue(message, Device.class);

           if(device.getMethod().equals("insert")){
               deviceService.insert(device);
           }

            if(device.getMethod().equals("update")){
                deviceService.update(device.getId(), device);
            }

            if(device.getMethod().equals("delete")){
                deviceService.delete(device.getId());
            }



        } catch(JsonProcessingException e){
            LOGGER.error("Cannot convert message");
        }
    }


}
