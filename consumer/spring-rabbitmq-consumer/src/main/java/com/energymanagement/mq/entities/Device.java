package com.energymanagement.mq.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Data;


@Entity
@Table(name = "DEVICE")
public class Device{

    @Id
    private Long id;

    @Column(name = "max_hourly_energy_consumption", nullable = false)
    private double max_hourly_energy_consumption;

    @Column(name = "userId", nullable = false)
    private Long userId;

    private String method;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public double getMax_hourly_energy_consumption() {
        return max_hourly_energy_consumption;
    }

    public void setMax_hourly_energy_consumption(double max_hourly_energy_consumption) {
        this.max_hourly_energy_consumption = max_hourly_energy_consumption;
    }

    public Device(Long id, double max_hourly_energy_consumption, Long userId) {
        this.id = id;
        this.max_hourly_energy_consumption = max_hourly_energy_consumption;
        this.userId = userId;

    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Device() {
    }

    public Device(@JsonProperty("id") Long id,
                  @JsonProperty("max_hourly_energy_consumption") double max_hourly_energy_consumption,
                  @JsonProperty("user_id") Long userId,
                  @JsonProperty("method") String method) {
        this.id = id;
        this.max_hourly_energy_consumption = max_hourly_energy_consumption;
        this.userId = userId;
        this.method = method;
    }
}

