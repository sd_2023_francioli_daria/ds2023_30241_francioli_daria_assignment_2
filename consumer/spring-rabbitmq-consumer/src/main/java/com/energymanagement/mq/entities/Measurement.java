package com.energymanagement.mq.entities;

import jakarta.persistence.*;


@Entity
@Table(name = "measurement")
public class Measurement {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "timestamp", nullable = false)
    private String timestamp;

    @Column(name = "measurement_value", nullable = false)
    private double measurement_value;

    @Column(name = "device_id", nullable = false)
    private Long device_id;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Long getDevice_id() {
        return device_id;
    }

    public void setDevice_id(Long device_id) {
        this.device_id = device_id;
    }

    public double getMeasurement_value() {
        return measurement_value;
    }

    public void setMeasurement_value(double measurement_value) {
        this.measurement_value = measurement_value;
    }

    public Measurement(String timestamp, double measurement_value, Long device_id) {
        this.timestamp = timestamp;
        this.measurement_value = measurement_value;
        this.device_id = device_id;
    }

    public Measurement() {
    }
}
