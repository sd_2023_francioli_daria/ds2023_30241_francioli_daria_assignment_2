package com.energymanagement.mq.services;

import com.energymanagement.mq.entities.Device;
import com.energymanagement.mq.repositories.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DeviceService {

    DeviceRepository deviceRepository;

    @Autowired
    public DeviceService(DeviceRepository deviceRepository){
        this.deviceRepository = deviceRepository;
    }

    public List<Device> findAll() {
        List<Device> devices = deviceRepository.findAll();
        return devices;
    }


    public Device findById(Long id) {
        Optional<Device> deviceOptional = deviceRepository.findById(id);
        if (!deviceOptional.isPresent()) {
            throw new RuntimeException(Device.class.getSimpleName() + " with id: " + id);
        }
        return deviceOptional.get();
    }

    public Device insert(Device device  ) {
        Device newDevice = deviceRepository.save(device);
        if(newDevice==null)
        {
            System.out.println("error");
        }
        return newDevice;
    }

    public Device update(Long id, Device device) {
        Optional<Device> deviceOptional = deviceRepository.findById(id);
        if(deviceOptional.isEmpty()){
            return null;
        }
        Device updateDevice = deviceOptional.get();
        updateDevice.setMax_hourly_energy_consumption(device.getMax_hourly_energy_consumption());
        updateDevice.setUserId(device.getUserId());
        updateDevice = deviceRepository.save(updateDevice);
        return updateDevice;
    }

    public Device delete(Long id) {
        Device deleteDevice = findById(id);
        if (deleteDevice!=null)
        {
            deviceRepository.delete(deleteDevice);
        }
        return deleteDevice;
    }

}
