package com.energymanagement.mq.entities;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.io.Serializable;

public class CustomMessage implements Serializable {


    private double measurement_value;

    private String timestamp;

    private Long device_id;

    public CustomMessage(@JsonProperty("measurement_value") double measurement_value,
                         @JsonProperty("timestamp") String timestamp,
                         @JsonProperty("device_id") Long device_id) {
        this.measurement_value = measurement_value;
        this.timestamp = timestamp;
        this.device_id = device_id;
    }

    public CustomMessage() {
    }

    public double getMeasurement_value() {
        return measurement_value;
    }

    public void setMeasurement_value(double measurement_value) {
        this.measurement_value = measurement_value;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Long getDevice_id() {
        return device_id;
    }

    public void setDevice_id(Long device_id) {
        this.device_id = device_id;
    }
}
