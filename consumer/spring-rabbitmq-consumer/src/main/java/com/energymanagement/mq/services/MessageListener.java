package com.energymanagement.mq.services;

import com.energymanagement.mq.config.MQConfig;
import com.energymanagement.mq.entities.CustomMessage;
import com.energymanagement.mq.entities.Device;
import com.energymanagement.mq.entities.Measurement;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class MessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageListener.class);
    private final MeasurementService measurementService;
    private final DeviceService deviceService;
    private int count = 0;
    private double average = 0;

    private final SimpMessagingTemplate messagingTemplate;

    @Autowired
    public MessageListener(MeasurementService measurementService, DeviceService deviceService, SimpMessagingTemplate messagingTemplate) {
        this.measurementService = measurementService;
        this.deviceService = deviceService;
        this.messagingTemplate = messagingTemplate;
    }


    @RabbitListener(queues = MQConfig.QUEUE)
    public void listener(String message)  {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            CustomMessage customMessage = objectMapper.readValue(message, CustomMessage.class);

            Measurement measurement = new Measurement(customMessage.getTimestamp(), customMessage.getMeasurement_value(), customMessage.getDevice_id());

            measurementService.insert(measurement);

            Device device = deviceService.findById(customMessage.getDevice_id());

            average += customMessage.getMeasurement_value();
            count ++;

            if(count == 6)
            {
                average /= 6;
                if(average > device.getMax_hourly_energy_consumption()){
                    notification(customMessage);
                }
                count = 0;
                average = 0;
            }

            System.out.println(message);

        } catch(JsonProcessingException e){
            LOGGER.error("Cannot convert message");
        }
    }

    @Async
    public void notification(CustomMessage customMessage) {
        LOGGER.error("Threshold exceeded for device with device id " + customMessage.getDevice_id() + " at " + customMessage.getTimestamp());

        Map<String, Object> notificationData = new HashMap<>();
        notificationData.put("deviceId", customMessage.getDevice_id());
        notificationData.put("message", "Threshold exceeded for device " + customMessage.getDevice_id());
        notificationData.put("average", average); // If you want to send the average as well

        // Send the notification to the "/topic" channel
        messagingTemplate.convertAndSend("/topic", notificationData);
    }

}