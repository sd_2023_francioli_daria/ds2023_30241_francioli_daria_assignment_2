package com.energymanagement.mq;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@RestController
public class MessagePublisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessagePublisher.class);

    @Autowired
    private RabbitTemplate template;

//    public String publishMessage() {
//        CustomMessage message = new CustomMessage();
//        message.setDevice_id(1L);
//        message.setTimestamp(LocalDateTime.now().toString());
//        message.setMeasurement_value(1);
//
//        ObjectMapper mapper = new ObjectMapper();
//        String jsonMessage;
//        try {
//            jsonMessage = mapper.writeValueAsString(message);
//        } catch ( JsonProcessingException e) {
//            LOGGER.error("Error converting message to JSON", e);
//            return "Error Publishing Message";
//        }
//
//        template.convertAndSend(MQConfig.EXCHANGE, MQConfig.ROUTING_KEY, jsonMessage);
//        LOGGER.error(jsonMessage);
//        return "Message Published";
//    }

    @PostMapping("/publish")
    public String publishMessagesFromCsv() {
        String csvFilePath = "C:\\Facultate AN 4 SEM1\\SD\\Assigment 2\\producer\\spring-rabbitmq-producer\\src\\main\\resources\\sensor.csv";
        try (BufferedReader br = new BufferedReader(new FileReader(csvFilePath))) {
            String line;

            // Read each line from the CSV
            while ((line = br.readLine()) != null) {
                try {
                    // Parse the measurement value
                    double measurementValue = Double.parseDouble(line);

                    // Create and populate the message object
                    CustomMessage message = new CustomMessage();
                    message.setDevice_id(1L); // Assuming a constant device ID
                    message.setTimestamp(LocalDateTime.now().toString());
                    message.setMeasurement_value(measurementValue);

                    // Convert message to JSON
                    ObjectMapper mapper = new ObjectMapper();
                    String jsonMessage = mapper.writeValueAsString(message);

                    // Send the message
                    template.convertAndSend(MQConfig.EXCHANGE, MQConfig.ROUTING_KEY, jsonMessage);
                    LOGGER.info("Message Published: " + jsonMessage);

                } catch (JsonProcessingException e) {
                    LOGGER.error("Error converting message to JSON", e);
                } catch (NumberFormatException e) {
                    LOGGER.error("Error parsing measurement value from CSV", e);
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error reading CSV file", e);
            return "Error Publishing Messages";
        }

        return "Messages Published";
    }


    @PostMapping("/publish2")
    public String publishMessagesFromCsv2() {
        String csvFilePath = "C:\\Facultate AN 4 SEM1\\SD\\Assigment 2\\producer\\spring-rabbitmq-producer\\src\\main\\resources\\sensor.csv";
        try (BufferedReader br = new BufferedReader(new FileReader(csvFilePath))) {
            String line;

            // Read each line from the CSV
            while ((line = br.readLine()) != null) {
                try {
                    // Parse the measurement value
                    double measurementValue = Double.parseDouble(line);

                    // Create and populate the message object
                    CustomMessage message = new CustomMessage();
                    message.setDevice_id(10L); // Assuming a constant device ID
                    message.setTimestamp(LocalDateTime.now().toString());
                    message.setMeasurement_value(measurementValue);

                    // Convert message to JSON
                    ObjectMapper mapper = new ObjectMapper();
                    String jsonMessage = mapper.writeValueAsString(message);

                    // Send the message
                    template.convertAndSend(MQConfig.EXCHANGE, MQConfig.ROUTING_KEY, jsonMessage);
                    LOGGER.info("Message Published: " + jsonMessage);

                } catch (JsonProcessingException e) {
                    LOGGER.error("Error converting message to JSON", e);
                } catch (NumberFormatException e) {
                    LOGGER.error("Error parsing measurement value from CSV", e);
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error reading CSV file", e);
            return "Error Publishing Messages";
        }

        return "Messages Published";
    }

}