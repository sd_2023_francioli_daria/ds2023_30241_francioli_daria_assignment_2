package com.energymanagement.mq;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize
public class CustomMessage {

    @JsonProperty("measurement_value")
    private double measurement_value;
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("device_id")
    private Long device_id;
}